import random

class Stone(object):
    pass

class Person(object):
    def __init__(self,name):
        self.strength = random.randint(3,18)
        self.health = random.randint(80,100)
        self.sword_sharpness = 1.0
        self.name = name

    def __add__(self, obj):
        if type(obj) == Person:
            self.attack(obj)
        elif type(obj) == Stone:
            self.sharpen_sword()

    def attack(self, person):
        person.health -= self.strength * self.sword_sharpness
        self.sword_sharpness -= 0.1
        print person.name + " is down to %s health" % person.health

        self.health -= person.strength * person.sword_sharpness
        person.sword_sharpness -= 0.1
        print self.name + " is down to %s health" % self.health
    
    def sharpen_sword(self):
        self.sword_sharpness += 0.2
        print self.name + " has sharpened his sword to %s sharpness" % self.sword_sharpness
        
zach = Person('zach')
taavi = Person('taavi')
big_rock = Stone()

taavi+ zach
taavi+ zach
zach+ big_rock
zach+ taavi

# zach is down to 84.0 health
# taavi is down to 79.0 health
# zach is down to 81.3 health
# taavi is down to 73.6 health
# zach has sharpened his sword to 1.0 sharpness
# taavi is down to 67.6 health
# zach is down to 78.9 health
